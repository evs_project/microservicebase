﻿using System;
using System.Collections.Generic;

namespace AEG.Data.Context
{
    public partial class Roles
    {
        public Roles()
        {
            Users = new HashSet<Users>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public ulong IsActive { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
