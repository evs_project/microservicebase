﻿using System;
using System.Collections.Generic;

namespace AEG.Data.Context
{
    public partial class Users
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public int? RoleId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public ulong IsActive { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public DateTime? TokenExpiry { get; set; }
        public ulong? RememberMe { get; set; }
        public string Title { get; set; }

        public virtual Roles Role { get; set; }
    }
}
