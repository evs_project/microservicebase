﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AEG.Data.Interfaces
{
    /// <summary>
    /// IGenericRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(object id);

        /// <summary>
        /// Finds the asynchronous.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Finds the by asyn.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        /// <summary>
        /// Finds the by query.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<T> FindByQuery(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        /// <summary>
        /// Inserts the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        Task InsertAsync(T obj);

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        void BulkInsert(IEnumerable<T> entities);

        /// <summary>
        /// BulkInsert
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        void BulkUpdate(IEnumerable<T> entities);

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void Update(T obj);

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        void Dispose();

        /// <summary>
        /// Deletes the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void Delete(T obj);

        /// <summary>
        /// Excutes the SQL query asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <returns></returns>
        Task<ICollection> ExcuteSqlQueryAsync(string sqlQuery, object Params = null, CommandType commandType = CommandType.StoredProcedure);

        /// <summary>
        /// Excutes the SQL query generic asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <returns></returns>
        Task<IEnumerable<T>> ExcuteSqlQueryGenericAsync(string sqlQuery, object Params = null);
    }
}