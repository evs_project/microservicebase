﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AEG.Data.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public interface IUnitOfWork<out TContext>
          where TContext : DbContext, new()
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        TContext Context { get; }

        /// <summary>
        /// Creates the transaction.
        /// </summary>
        void CreateTransaction();

        /// <summary>
        /// Commits this instance.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        void Rollback();

        /// <summary>
        /// Saves this instance.
        /// </summary>
        Task SaveAsync();
    }
}