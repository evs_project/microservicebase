﻿using AEG.Data.Interfaces;
using AEG.Data.Context;
using Dapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AEG.Data.Repository
{
    /// <summary>
    /// GenericRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="EVS.UnderWriting.Data.Interface.IGenericRepository{T}" />
    /// <seealso cref="System.IDisposable" />
    public class GenericRepository<T> : IGenericRepository<T>, IDisposable where T : class
    {
        private DbSet<T> _entities;
        private bool _isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="unitofwork">The unitofwork.</param>
        public GenericRepository(IUnitOfWork<AcquityEyeDbContext> unitofwork)
        {
            _isDisposed = false;
            Context = unitofwork.Context;
        }

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public AcquityEyeDbContext Context { get; set; }

        /// <summary>
        /// Gets the table.
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        public virtual IQueryable<T> Table
        {
            get { return Entities; }
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <value>
        /// The entities.
        /// </value>
        protected virtual DbSet<T> Entities
        {
            get { return _entities ?? (_entities = Context.Set<T>()); }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (Context != null)
                Context.Dispose();
            _isDisposed = true;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Entities.ToListAsync();
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public virtual async Task<T> GetByIdAsync(object id)
        {
            return await Entities.FindAsync(id);
        }

        /// <summary>
        /// Finds the asynchronous.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate)
        {
            return await Entities.AsNoTracking().Where(predicate).ToListAsync();
        }

        /// <summary>
        /// Finds the by asyn.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var filteredExpression = Entities.Where(predicate);
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                filteredExpression = filteredExpression.Include(includeProperty);
            }
            return await filteredExpression.ToListAsync();
        }

        /// <summary>
        /// Finds the by query asyn.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public virtual IQueryable<T> FindByQuery(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var filteredExpression = Entities.Where(predicate);
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                filteredExpression = filteredExpression.Include(includeProperty);
            }
            return filteredExpression;
        }

        /// <summary>
        /// Inserts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="ArgumentNullException">entity</exception>
        /// <exception cref="Exception"></exception>
        public virtual async Task InsertAsync(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                await Entities.AddAsync(entity);
                if (Context == null || _isDisposed)
                    Context = new AcquityEyeDbContext();
                //Context.SaveChanges(); commented out call to SaveChanges as Context save changes will be
                //called with Unit of work
            }
            catch (ValidationException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
        }

        /// <summary>
        /// Bulks the insert.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <exception cref="ArgumentNullException">entities</exception>
        /// <exception cref="Exception"></exception>
        public virtual void BulkUpdate(IEnumerable<T> entities)
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                Context.ChangeTracker.AutoDetectChangesEnabled = false;
                Context.Set<T>().UpdateRange(entities);
                //Context.SaveChanges();
            }
            catch (ValidationException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
        }

        /// <summary>
        /// Bulks the insert.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <exception cref="ArgumentNullException">entities</exception>
        /// <exception cref="Exception"></exception>
        public virtual void BulkInsert(IEnumerable<T> entities)
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                Context.ChangeTracker.AutoDetectChangesEnabled = false;
                Context.Set<T>().AddRange(entities);
                //Context.SaveChanges();
            }
            catch (ValidationException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="ArgumentNullException">entity</exception>
        /// <exception cref="Exception"></exception>
        public virtual void Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                if (Context == null || _isDisposed)
                    Context = new AcquityEyeDbContext();
                SetEntryModified(entity);
                //Context.SaveChanges(); commented out call to SaveChanges as Context save changes will be called with Unit of work
            }
            catch (ValidationException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="ArgumentNullException">entity</exception>
        /// <exception cref="Exception"></exception>
        public virtual void Delete(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                if (Context == null || _isDisposed)
                    Context = new AcquityEyeDbContext();
                Entities.Remove(entity);
                //Context.SaveChanges(); //commented out call to SaveChanges as Context save changes will be called with Unit of work
            }
            catch (ValidationException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
        }

        /// <summary>
        /// Sets the entry modified.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public virtual void SetEntryModified(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Excutes the SQL query asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <returns></returns>
        public async Task<ICollection> ExcuteSqlQueryAsync(string sqlQuery, object Params = null, CommandType commandType = CommandType.StoredProcedure)
        {
            if (commandType == CommandType.Text)
            {
                return await SqlQueryAsync(sqlQuery, Params);
            }
            else if (commandType == CommandType.StoredProcedure)
            {
                return await StoredProcedureAsync(sqlQuery, Params);
            }

            return null;
        }

        /// <summary>
        /// Excutes the SQL query generic asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> ExcuteSqlQueryGenericAsync(string sqlQuery, object Params = null)
        {
            var result = await Context.Database.GetDbConnection().QueryAsync<T>(sqlQuery, Params, null, null, CommandType.StoredProcedure);
            return result.ToList();
        }

        private async Task<ICollection> SqlQueryAsync(string sqlQuery, object Params = null)
        {
            var result = await Context.Database.GetDbConnection().QueryAsync(sqlQuery, Params);
            return result.ToList();
        }

        private async Task<ICollection> StoredProcedureAsync(string storedProcedureName, object Params = null)
        {
            var result = await Context.Database.GetDbConnection().QueryAsync(storedProcedureName, Params, null, null, CommandType.StoredProcedure);
            return result.ToList();
        }
    }
}