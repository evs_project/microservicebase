﻿using AEG.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace AEG.Data.Repository
{
    /// <summary>
    /// UnitOfWork
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    /// <seealso cref="IUnitOfWork{TContext}" />
    /// <seealso cref="System.IDisposable" />
    public class UnitOfWork<TContext> : IUnitOfWork<TContext>, IDisposable
 where TContext : DbContext, new()
    {
        private bool _disposed;
        private IDbContextTransaction _objTran;
        private Dictionary<string, object> _repositories;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork{TContext}"/> class.
        /// </summary>
        public UnitOfWork()
        {
            Context = new TContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork{TContext}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UnitOfWork(TContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public TContext Context { get; }

        /// <summary>
        /// Creates the transaction.
        /// </summary>
        public void CreateTransaction()
        {
            _objTran = Context.Database.BeginTransaction();
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        public void Commit()
        {
            _objTran.Commit();
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        public void Rollback()
        {
            _objTran.Rollback();
            _objTran.Dispose();
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <exception cref="Exception"></exception>
        public async Task SaveAsync()
        {
            try
            {
                await Context.SaveChangesAsync();
            }
            catch (ValidationException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    Context.Dispose();
            _disposed = true;
        }

        /// <summary>
        /// Generics the repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public GenericRepository<T> GenericRepository<T>() where T : class
        {
            if (_repositories == null)
                _repositories = new Dictionary<string, object>();
            var type = typeof(T).Name;
            if (!_repositories.ContainsKey(type))
            {
                var repositoryType = typeof(GenericRepository<T>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), Context);
                _repositories.Add(type, repositoryInstance);
            }
            return (GenericRepository<T>)_repositories[type];
        }
    }
}