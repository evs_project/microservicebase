﻿namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// IBusinessLayer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="AEG.Logic.Interfaces.ISave{T}" />
    /// <seealso cref="AEG.Logic.Interfaces.IDelete{T}" />
    /// <seealso cref="AEG.Logic.Interfaces.IGet{T}" />
    /// <seealso cref="AEG.Logic.Interfaces.IGetAll{T}" />
    /// <seealso cref="AEG.Logic.Interfaces.IUpdate{T}" />
    /// <seealso cref="AEG.Logic.Interfaces.ITransaction" />
    /// <seealso cref="AEG.Logic.Interfaces.IGetProc{T}" />
    public interface IBusinessLayer<T> : ISave<T>, IDelete<T>, IGet<T>, IGetAll<T>, IUpdate<T>, ITransaction, IGetProc<T>
    {
    }
}