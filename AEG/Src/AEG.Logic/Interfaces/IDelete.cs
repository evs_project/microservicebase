﻿using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// IDelete
    /// </summary>
    public interface IDelete<T>
    {
        /// <summary>
        /// Deletes the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        Task Delete(T obj);
    }
}