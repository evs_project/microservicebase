﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// IGet
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGet<T>
    {
        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<T> Get(object id);

        /// <summary>
        /// Finds the by asyn.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Finds the by asyn.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        /// <summary>
        /// Finds the by query.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<T> FindByQuery(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
    }
}