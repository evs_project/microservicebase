﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// IGetAll
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGetAll<T>
    {
        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();
    }
}