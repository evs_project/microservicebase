﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// IGetProc
    /// </summary>
    public interface IGetProc<T>
    {
        /// <summary>
        /// Excutes the SQL query asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <returns></returns>
        Task<ICollection> ExcuteSqlQueryAsync(string sqlQuery, object Params = null, CommandType commandType = CommandType.StoredProcedure);

        /// <summary>
        /// Excutes the SQL query generic asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <returns></returns>
        Task<IEnumerable<T>> ExcuteSqlQueryGenericAsync(string sqlQuery, object Params = null);
    }
}