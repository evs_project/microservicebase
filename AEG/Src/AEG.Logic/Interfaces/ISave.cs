﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// ISave
    /// </summary>
    public interface ISave<T>
    {
        /// <summary>
        /// Saves the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        Task Save(T obj);

        /// <summary>
        /// Transactions the save.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        Task TransactionSave(T obj);

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Task BulkSave(IEnumerable<T> obj);
    }
}