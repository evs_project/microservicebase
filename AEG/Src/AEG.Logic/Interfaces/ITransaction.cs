﻿using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// ITransaction
    /// </summary>
    public interface ITransaction
    {
        /// <summary>
        /// Starttransactions this instance.
        /// </summary>
        void Starttransaction();

        /// <summary>
        /// Committransactions this instance.
        /// </summary>
        /// <returns></returns>
        Task Committransaction();

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        void RollbackTransaction();
    }
}