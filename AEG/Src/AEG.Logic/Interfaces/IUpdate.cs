﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AEG.Logic.Interfaces
{
    /// <summary>
    /// IUpdate
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IUpdate<T>
    {
        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        Task Update(T obj);

        /// <summary>
        /// Transactions the update.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        void TransactionUpdate(T obj);

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Task BulkUpdate(IEnumerable<T> obj);
    }
}