﻿using AEG.Data.Interfaces;
using AEG.Data.Context;
using AEG.Logic.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AEG.Logic.Logic
{
    /// <summary>
    /// BusinessLayerConcrete
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="IMasterBusinessLayer{T}" />
    /// <seealso cref="System.IDisposable" />
    public class BusinessLayerConcrete<T> : IBusinessLayer<T>, IDisposable where T : class
    {
        private readonly IGenericRepository<T> _genericRepository;
        private readonly IUnitOfWork<AcquityEyeDbContext> _unitOfWork;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterBusinessLayerConcrete{T}"/> class.
        /// </summary>
        /// <param name="genericRepository">The generic repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public BusinessLayerConcrete(IGenericRepository<T> genericRepository, IUnitOfWork<AcquityEyeDbContext> unitOfWork)
        {
            IsDisposed = false;
            _genericRepository = genericRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Dispose()
        {
            if (_genericRepository != null)
                _genericRepository.Dispose();
            IsDisposed = true;
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<T> Get(object id)
        {
            return await _genericRepository.GetByIdAsync(id);
        }

        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _genericRepository.GetAllAsync();
        }

        /// <summary>
        /// Finds the asynchronous.
        /// </summary>
        /// <param name="predicate">The expression.</param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate)
        {
            return await _genericRepository.FindByAsyn(predicate);
        }

        /// <summary>
        /// Finds the by asyn.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> FindByAsyn(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            return await _genericRepository.FindByAsyn(predicate, includeProperties);
        }

        /// <summary>
        /// Finds the by query.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<T> FindByQuery(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            return _genericRepository.FindByQuery(predicate, includeProperties);
        }

        /// <summary>
        /// Saves the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public async Task Save(T obj)
        {
            await _genericRepository.InsertAsync(obj);
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Bulk Saves the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public async Task BulkUpdate(IEnumerable<T> obj)
        {
            _genericRepository.BulkUpdate(obj);
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Bulk Saves the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public async Task BulkSave(IEnumerable<T> obj)
        {
            _genericRepository.BulkInsert(obj);
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public async Task Update(T obj)
        {
            _genericRepository.Update(obj);
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Saves the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public async Task TransactionSave(T obj)
        {
            await _genericRepository.InsertAsync(obj);
        }

        /// <summary>
        /// Transactions the update.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void TransactionUpdate(T obj)
        {
            _genericRepository.Update(obj);
        }

        /// <summary>
        /// Starttransactions this instance.
        /// </summary>
        public void Starttransaction()
        {
            _unitOfWork.CreateTransaction();
        }

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        public void RollbackTransaction()
        {
            _unitOfWork.Rollback();
        }

        /// <summary>
        /// Committransactions this instance.
        /// </summary>
        public async Task Committransaction()
        {
            await _unitOfWork.SaveAsync();
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Deletes the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public async Task Delete(T obj)
        {
            _genericRepository.Delete(obj);
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Excutes the SQL query asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <returns></returns>
        public async Task<ICollection> ExcuteSqlQueryAsync(string sqlQuery, object Params = null, CommandType commandType = CommandType.StoredProcedure)
        {
            return await _genericRepository.ExcuteSqlQueryAsync(sqlQuery, Params, commandType);
        }

        /// <summary>
        /// Excutes the SQL query generic asynchronous.
        /// </summary>
        /// <param name="sqlQuery">The SQL query.</param>
        /// <param name="Params">The parameters.</param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> ExcuteSqlQueryGenericAsync(string sqlQuery, object Params = null)
        {
            return await _genericRepository.ExcuteSqlQueryGenericAsync(sqlQuery, Params);
        }
    }
}