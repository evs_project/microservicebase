﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AEG.Data.Context;
using AEG.Common.CustomExceptions;
using AEG.Logic.Interfaces;

namespace AEG.PatientService.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IBusinessLayer<Users> _businessLayer;

        public UsersController(IBusinessLayer<Users> businessLayer)
        {
            _businessLayer = businessLayer;
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Users>> GetUsers()
        {
            return await _businessLayer.GetAllAsync().ConfigureAwait(true);
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Users>> GetUsers(int id)
        {
            var users = await _businessLayer.Get(id).ConfigureAwait(true);

            if (users == null)
            {
                return NotFound();
            }

            return users;
        }

        /// <summary>
        /// Puts the users.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="users">The users.</param>
        /// <exception cref="NotFoundException"></exception>
        [HttpPut("{id}")]
        public async Task PutUsers(int id, Users users)
        {
            if (id != users.UserId)
            {
                throw new NotFoundException();
            }

            await _businessLayer.Update(users).ConfigureAwait(true);
        }

        /// <summary>
        /// Posts the users.
        /// </summary>
        /// <param name="users">The users.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Users>> PostUsers(Users users)
        {
            await _businessLayer.Save(users).ConfigureAwait(true);
            return users;
        }

        /// <summary>
        /// Deletes the users.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Users>> DeleteUsers(int id)
        {
            var iso = await _businessLayer.Get(id).ConfigureAwait(true);
            if (iso == null)
            {
                return NotFound();
            }
            await _businessLayer.Delete(iso).ConfigureAwait(true);
            return iso;
        }
    }
}