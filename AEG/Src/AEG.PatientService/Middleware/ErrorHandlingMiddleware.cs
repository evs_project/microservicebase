﻿using AEG.Common.CustomExceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AEG.PatientService.Middleware
{
    /// <summary>
    /// ErrorHandlingMiddleware
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorHandlingMiddleware"/> class. Test comment
        /// </summary>
        /// <param name="next">The next.</param>
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Invokes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <exception cref="NotFoundException"></exception>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);

                if (context.Response.StatusCode == 404)
                    throw new NotFoundException(string.Format("Http verb={0}, URL={1}", context.Request.Method, context.Request.Path));
            }
            catch (Exception ex)
            {
                _ = HandleException(context, ex);
            }
        }

        private static Task HandleException(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = 400;
            return context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                error = (exception.InnerException != null) ? exception.InnerException.Message : exception.Message
            }));
        }
    }
}