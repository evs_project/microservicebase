﻿namespace AEG.Common.Consts
{
    /// <summary>
    /// AppSettings
    /// </summary>
    public static class AppSettings
    {
        /// <summary>
        /// The JWT issuer
        /// </summary>
        public const string JwtIssuer = "Jwt:Issuer";

        /// <summary>
        /// The JWT key
        /// </summary>
        public const string JwtKey = "Jwt:Key";

        /// <summary>
        /// The email SMTP host
        /// </summary>
        public const string EmailSmtpHost = "Email:Smtp:Host";

        /// <summary>
        /// The email SMTP port
        /// </summary>
        public const string EmailSmtpPort = "Email:Smtp:Port";

        /// <summary>
        /// The email SMTP username
        /// </summary>
        public const string EmailSmtpUsername = "Email:Smtp:Username";

        /// <summary>
        /// The email SMTP password
        /// </summary>
        public const string EmailSmtpPassword = "Email:Smtp:Password";

        /// <summary>
        /// The database connection string
        /// </summary>
        public const string DbConnectionString = "DbConnectionString";
    }
}